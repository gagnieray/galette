{#
/**
 * Copyright © 2003-2024 The Galette Team
 *
 * This file is part of Galette (https://galette.eu).
 *
 * Galette is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Galette is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Galette. If not, see <http://www.gnu.org/licenses/>.
 */
#}
{% macro renderMenu(title, icon, items, mode) %}
    {% set my_routes = [] %}
    {% for item in items %}
        {% if item.route is defined %}
            {% set my_routes = my_routes|merge([item.route.name])|merge(item.route.aliases ?? []) %}
        {% elseif item.children is defined %}
            {% for child in item.children %}
                {% set my_routes = my_routes|merge([child.route.name])|merge(child.route.aliases ?? []) %}
            {% endfor %}
        {% endif %}
    {% endfor %}
    {% if mode == "compact" %}
        <div class="ui{% if cur_route in my_routes %} active-menu{% endif %} dropdown navigation item tooltip" data-html="{{ title }}" data-position="right center">
            <i class="{{ icon }} icon" aria-hidden="true"></i>
            <span class="visually-hidden">{{ title }}</span>
            <i class="dropdown icon" aria-hidden="true"></i>
            <div class="menu">
                {% for item in items %}
                    {{ _self.renderMenuItem(item.label, item.title ?? null, item.route ?? null, null, null, 'right center', mode, item.children ?? null) }}
                {% endfor %}
            </div>
        </div>
    {% else %}
        <div class="item">
            <div class="image header title{% if cur_route in my_routes %} active{% endif %}" data-fold="fold-{{ icon|replace({' ': '-'}) }}" tabindex="0">
                <i class="{{ icon }} icon" aria-hidden="true"></i>
                {{ title }}
                <i class="dropdown icon" aria-hidden="true"></i>
            </div>
            <div class="content{% if cur_route in my_routes %} active{% endif %}">
                {% for item in items %}
                    {{ _self.renderMenuItem(item.label, item.title ?? null, item.route ?? null, item.icon ?? null, null, null, mode, item.children ?? null) }}
                {% endfor %}
            </div>
        </div>
    {% endif %}
{% endmacro %}

{% macro renderMenuItem(label, title, route, icon, class, tips_position, mode, children) %}
    {% if class is empty %}
        {% if route is not null %}
            {% set my_routes = [route.name]|merge(route.aliases ?? []) %}
            {% if is_current_url(route.name, route.args ?? [] + cur_route_args ?? []) %}
                {% set class = "active item" %}
            {% else %}
                {% set class = "item" %}
            {% endif %}
        {% else %}
            {% set class = "item" %}
        {% endif %}
    {% endif %}
    {% if children is empty %}
        <a
                href="{{ url_for(route.name, route.args|default([])) }}"
                class="{{ class }}"
                {% if title %}title="{{ title }}"{% endif %}
                {% if tips_position %}data-position="{{ tips_position }}"{% endif %}
                {% if mode == "compact" %}tabindex="-1"{% endif %}
        >
        {% if icon and mode != 'compact' %}
            <i class="{{ icon }} icon" aria-hidden="true"></i>
        {% endif %}
            {{ label }}
        </a>
    {% elseif (mode != 'topbar') and login.isLogged() %}
        {% for item in children %}
            {% set icon = mode == "compact" ? null : item.icon %}
            {{ _self.renderMenuItem(item.label, item.title ?? null, item.route, icon, null, item.tips_position ?? null) }}
        {% endfor %}
    {% else %}
        {{ _self.renderMenuDropdown(label, title ?? null, icon ?? null, tips_position ?? null, mode, children) }}
    {% endif %}
{% endmacro %}

{% macro renderMenuDropdown(label, title, icon, tips_position, mode, children) %}
    {% set my_routes = [] %}
    {% for item in children %}
        {% set my_routes = my_routes|merge([item.route.name])|merge(item.route.aliases ?? []) %}
    {% endfor %}
    <div class="{{ mode != 'sidebar' ? 'focus-visible ui dropdown navigation ' }}{% if cur_route in my_routes %} active-menu {% endif %}item">
    {% if mode == 'sidebar' %}
        <div class="image header title" data-fold="fold-{{ icon|replace({' ': '-'}) }}">
    {% endif %}
            <i class="{{ icon }} icon" aria-hidden="true"></i>
            <span>{{ label }}</span>
            <i class="dropdown icon" aria-hidden="true"></i>
    {% if mode == 'sidebar' %}
        </div>
    {% endif %}
        <div class="{{ mode != 'sidebar' ? 'menu' : 'content' }}{{ mode == 'sidebar' and cur_route in my_routes ?? ' active' }}">
            {% for item in children %}
                {{ _self.renderMenuItem(item.label, item.title ?? null, item.route, null, null, null, mode) }}
            {% endfor %}
        </div>
    </div>
{% endmacro %}

{% macro dashboardCard(label, title, route, icon) %}
    <a class="ui card" href="{{ url_for(route.name, route.args|default([])) }}"{% if title %} title="{{ title }}"{% endif %}>
        <div class="content">
            <div class="ui header">
                <em data-emoji="{{ icon }}" class="medium" aria-hidden="true"></em>
                <div class="content">
                    {{ label }}
                </div>
            </div>
        </div>
    </a>
{% endmacro %}

{% macro drawListAction(title, route, icon, extra_class) %}
    <a
            href="{{ url_for(route.name, route.args|default([])) }}"
            class="{{ extra_class|default('') }} tooltip"
            title="{{ title }}"
    >
        <i class="ui {{ icon }} icon" aria-hidden="true"></i>
        <span class="visually-hidden">{{ title }}</span>
    </a>
{% endmacro %}

{% macro drawDetailedAction(label, title, route, icon) %}
    <a
        href="{{ url_for(route.name, route.args|default([])) }}"
        title="{{ title }}"
        class="ui item"
    >
        <i class="{{ icon }} icon" aria-hidden="true"></i>
        {{ label }}
    </a>
{% endmacro %}

{% macro drawBatchActionsList(batch_actions) %}
    <div class="checkboxes ui basic horizontal segments">
        <div class="ui basic fitted segment batch-selection">
            <div class="ui blue tertiary dropdown icon button batch-select-action">
                <i class="tasks icon" aria-hidden="true"></i>
                {{ _T('For the selection:') }}
                <i class="dropdown icon" aria-hidden="true"></i>
                <div class="menu">
                {% for batch_action in batch_actions %}
                    {{ _self.drawBatchAction(batch_action.name, batch_action.label, batch_action.icon, batch_action.title ?? null) }}
                {% endfor %}
                </div>
            </div>
        </div>
    </div>
{% endmacro %}

{% macro drawBatchAction(name, label, icon, title) %}
    <span class="ui item batch-action batch-{{ name }}" data-value="{{ name }}"{% if title %} title="{{ title }}"{% endif %}>
        <i class="{{ icon }} icon" aria-hidden="true"></i> {{ label }}
    </span>
{% endmacro %}
